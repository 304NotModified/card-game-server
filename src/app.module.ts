import { Module } from '@nestjs/common';
import { TableModule } from './table/table.module';
import { IdModule } from './id/id.module';
import { AppController } from './app.controller';

@Module({
  imports: [TableModule, IdModule],
  controllers: [AppController],
  providers: []
})
export class AppModule {}
