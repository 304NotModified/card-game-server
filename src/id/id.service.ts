import { Injectable } from '@nestjs/common';

@Injectable()
export class IdService {

    private usedIds: string[] = [];

    public createId(): string {
        let uuid: string;
        do {
            uuid = this.uuidv4();
        } while (this.usedIds.indexOf(uuid) !== -1);
        this.usedIds.push(uuid);
        return uuid;
    }

    private uuidv4(): string {
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, (c) => {
          const r = Math.random() * 16 | 0;
          const v = c == 'x' ? r : (r & 0x3 | 0x8);
          return v.toString(16);
        });
      }
      
}
