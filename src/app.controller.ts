import { Controller, Get, Res } from '@nestjs/common';
import { ANGULAR_ROOT_DIR } from './app.constant';

@Controller()
export class AppController {

    // You'll need a file "app.constant.ts" next to this file.
    //  To serve a frontend application, let it's content be:
    // 
    // export const ANGULAR_ROOT_DIR = "<path-to-dist>/<folder-of-angular-application>";
    // 
    private rootDir = { root: ANGULAR_ROOT_DIR };

    @Get('app')
    public getAngularApp(@Res() res): any {
        res.sendFile('index.html', this.rootDir);
    }
    @Get('favicon.ico')
    public getFavicon(@Res() res): any {
        res.sendFile('favicon.ico', this.rootDir);
    }
    @Get('main-es5.a555c2b1b27b2f30efe6.js')
    public getMainES5(@Res() res): any {
        res.sendFile('main-es5.a555c2b1b27b2f30efe6.js', this.rootDir);
    }
    @Get('main-es2015.a555c2b1b27b2f30efe6.js')
    public getMainES2015(@Res() res): any {
        res.sendFile('main-es2015.a555c2b1b27b2f30efe6.js', this.rootDir);
    }
    @Get('polyfills-es5.1fd9b76218eca8053895.js')
    public getPolyfillsES5(@Res() res): any {
        res.sendFile('polyfills-es5.1fd9b76218eca8053895.js', this.rootDir);
    }
    @Get('polyfills-es2015.690002c25ea8557bb4b0.js')
    public getPolyfillsES2015(@Res() res): any {
        res.sendFile('polyfills-es2015.690002c25ea8557bb4b0.js', this.rootDir);
    }
    @Get('runtime-es5.1eba213af0b233498d9d.js')
    public getRuntimeES5(@Res() res): any {
        res.sendFile('runtime-es5.1eba213af0b233498d9d.js', this.rootDir);
    }
    @Get('runtime-es2015.1eba213af0b233498d9d.js')
    public getRuntimeES2015(@Res() res): any {
        res.sendFile('runtime-es2015.1eba213af0b233498d9d.js', this.rootDir);
    }
    @Get('styles.894d8f10718d51035a24.css')
    public getstyles(@Res() res): any {
        res.sendFile('styles.894d8f10718d51035a24.css', this.rootDir);
    }
}
