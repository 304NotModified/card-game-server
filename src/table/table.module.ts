import { Module } from '@nestjs/common';
import { TableController } from './table.controller';
import { TableService } from './table.service';
import { PlayerModule } from './player/player.module';
import { CardModule } from './card/card.module';
import { TableParser } from './table.parser';
import { TableSectionModule } from './table-section/table-section.module';

@Module({
    imports: [PlayerModule, CardModule, TableSectionModule],
    controllers: [TableController],
    providers: [TableService, TableParser],
    exports: [TableService, TableParser]
})
export class TableModule {}
