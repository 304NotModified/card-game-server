import { Controller, Post, HttpCode, Header, Get, Param, Body, Put, HttpException, HttpStatus, Delete } from '@nestjs/common';
import { TableService } from './table.service';
import { Table } from './table.class';
import { CreatePlayerDto } from './player/create-player.dto';
import { TableParser } from './table.parser';
import { UpdateTableDto } from './update-table-dto';

@Controller('table')
export class TableController {

    private sequence: number = 0;

    constructor(
        private tableService: TableService,
        private tableParser: TableParser
        ){}

    @Post()
    @Header('Cache-Control', 'none')
    async create(): Promise<any> {
        const id: string = this.tableService.create();
        console.log(`added table: ${id}`);
        return {"id": id};
    }

    @Post(':id')
    async addPlayer(@Param('id') id: string, @Body() createPlayerDto: CreatePlayerDto): Promise<any> {
        const playerId: string = this.tableService.addPlayerToTable(createPlayerDto, id);
        console.log(`player created with id: ${playerId}`);
        return {"id": playerId};
    }

    @Delete(':id/:playerId/:sequence')
    async removePlayer( @Param('id') id: string,
                        @Param('playerId') playerId: string,
                        @Param('sequence') sequence: string): Promise<void> {
        if (Number(sequence) !== this.sequence) {
            throw new HttpException(`Conflict: Trying to delete player for ${sequence}, but server is at ${this.sequence}`, HttpStatus.CONFLICT);
        }
        this.sequence = this.sequence + 1;
        this.tableService.findOne(id).removePlayer(playerId);
    }

    @Get(':id')
    @Header('Cache-Control', 'none')
    async findOne(@Param('id') id: string): Promise<any> {
        const table: Table = this.tableService.findOne(id);
        return {
            sequence: this.sequence,
            table: this.tableParser.objectToObject(table)
        };
    }

    @Put(':id/:sequence')
    async updateTable(@Param('id') id: string,
                      @Param('sequence') sequence: string,
                      @Body() updateTableDto: UpdateTableDto): Promise<void> {
        if (Number(sequence) !== this.sequence) {
            throw new HttpException(`Conflict: Got update for ${sequence}, but server is at ${this.sequence}`, HttpStatus.CONFLICT);
        }
        this.sequence = this.sequence + 1;
        this.tableService.updateTable(id, updateTableDto);
    }

    @Get()
    @Header('Cache-Control', 'none')
    async findAll(): Promise<any> {
        return this.tableService.findAll().map(table => this.tableParser.objectToObject(table));
    }
}
