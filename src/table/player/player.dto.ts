import { CardDto } from "../card/card.dto";

export class PlayerDto {
    public id: string;
    public name: string;
    public cards: CardDto[];
}