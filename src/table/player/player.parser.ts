import { Injectable, Logger } from "@nestjs/common";
import { Player } from "./player.class";
import { CardParser } from "../card/card.parser";

@Injectable()
export class PlayerParser {

    public constructor(private cardParser: CardParser) {}

    public mapToObject(players: Map<string, Player>): any {
        const obj = Object.create(null);
        for (let [id,player] of players) {
            obj[id] = this.objectToObject(player);
        }
        return obj;
    }

    public objectToObject(player: Player): any {
        return {
            id: player.id,
            name: player.name,
            cards: player.cards.map(card => this.cardParser.objectToObject(card))
        };
    }
}

// Monique         31 - 43 = 12
// Babs en Joe     30 - 41 = 11
// Vera            35 - 38 = 3
// Monica          29 -
// baby-op-schoot  27 -
// Phillip         30 - 36 = 6
// Leonie          28 - 36 = 8
// Hortente        27 - 33 = 6
// Vincent         23 - 32 = 9
// Mirjam          26 - 31.5 = 5.5
// Kim             26 - 30 = 4
// Elle en Tessa   16 - 26 = 10
// Danielle        22 - 24 = 2
