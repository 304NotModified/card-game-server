import { PlayerDto } from "./player/player.dto";
import { TableSectionDto } from "./table-section/table-section.dto";

export class UpdateTableDto {
    player: PlayerDto;
    mainSection: TableSectionDto;
    sideSection: TableSectionDto;
    playerSections: { [playerId: string]: TableSectionDto};
}