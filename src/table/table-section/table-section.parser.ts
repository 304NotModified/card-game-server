import { Injectable } from "@nestjs/common";
import { TableSection } from "./table-section.class";
import { CardParser } from "../card/card.parser";

@Injectable()
export class TableSectionParser {

    public constructor(private cardParser: CardParser) {}

    public objectToObject(tableSection: TableSection): any {
        return {
            openCards: tableSection.openCards.map(card => this.cardParser.objectToObject(card)),
            closedCards: tableSection.closedCards.map(card => this.cardParser.objectToObject(card))
        };
    }

    public mapToObject(tableSections: Map<string, TableSection>): string {
        const obj = Object.create(null);
        for (let [id,tableSection] of tableSections) {
            obj[id] = this.objectToObject(tableSection);
        }
        return obj;
    }
}