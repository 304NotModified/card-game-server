import { Injectable } from '@nestjs/common';
import { Table } from './table.class';
import { CreatePlayerDto } from './player/create-player.dto';
import { PlayerService } from './player/player.service';
import { UpdateTableDto } from './update-table-dto';

@Injectable()
export class TableService {
    private tables: Map<string, Table> = new Map();

    constructor(private playerService: PlayerService) {}

    public create(): string {
        let id: string;
        do {
            const num: number = Math.floor(Math.random() * 900000) + 100000;
            id = num.toString();
        } while(this.tables.has(id));
        this.tables.set(id, new Table(id));
        return id;
    }

    public addPlayerToTable(createPlayerDto: CreatePlayerDto, id: string): string {
        const player = this.playerService.createPlayer(createPlayerDto);
        this.findOne(id).addPlayer(player);
        return player.id;
    }

    public updateTable(id: string, updateTableDto: UpdateTableDto): void {
        this.findOne(id).update(updateTableDto);
    }

    public findAll(): Table[] {
        return Array.from(this.tables.values());
    }

    public findOne(id: string): Table {
        const table: Table = this.tables.get(id);
        return table;
    }
}
