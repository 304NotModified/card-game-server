import { Module } from '@nestjs/common';
import { CardService } from './card.service';
import { CardParser } from './card.parser';

@Module({
  providers: [CardService, CardParser],
  exports: [CardService, CardParser]
})
export class CardModule {}
