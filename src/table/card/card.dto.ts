import { Suit, CardName } from "./card.class"

export class CardDto {
    public suit: Suit;
    public name: CardName;
}